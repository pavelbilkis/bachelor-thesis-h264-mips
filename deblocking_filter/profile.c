#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "deblock_c.h"
#include "deblock_msa.h"

// number of test iterations
#define N_iter (10)
// number of macroblocks (N_MB x N_MB)
#define N_MB (4)

// Luma macroblock size 16x16 (do not change, this is supposed to be constant)
#define MB_luma_size (16)
// Chroma macroblock size 8x8 (do not change, this is supposed to be constant)
#define MB_chroma_size (8)

// Number of edges to filter
#define N_edges (4)

#define XSTRIDE_LUMA (N_MB*MB_luma_size)
#define XSTRIDE_CHROMA (N_MB*MB_chroma_size)

// working buffers
uint8_t Y[MB_luma_size * MB_luma_size * N_MB * N_MB];
uint8_t Cb[MB_chroma_size*MB_chroma_size*N_MB*N_MB];
uint8_t Cr[MB_chroma_size*MB_chroma_size*N_MB*N_MB];
int8_t c1[8] = {5, 5, 7, 6, 5, 4, 7, 8};

enum Edge {vertical, horizontal};

void generate_random_data()
{
    time_t t;
    srand((unsigned) time(&t));
    for (int i = 0; i < MB_luma_size * MB_luma_size * N_MB * N_MB; i++)
    {
        Y[i] = rand () % 128;
    }
    for (int i = 0; i < MB_chroma_size * MB_chroma_size * N_MB * N_MB; i++)
    {
        Cb[i] = rand () % 128;
        Cr[i] = rand () % 128;
    }
}

void deblock_area(int msa)
{
    uint8_t *Y_p_v = &Y[0], *Cb_p_v = &Cb[0], *Cr_p_v = &Cr[0];
    uint8_t *Y_p_h = &Y[0], *Cb_p_h = &Cb[0], *Cr_p_h = &Cr[0];
    uint8_t *Y_p, *Cb_p, *Cr_p;
    int Bs;
    int xstride_luma, ystride_luma, xstride_chroma, ystride_chroma;
    time_t t;
    srand((unsigned) time(&t));
    for (int i = 0; i < N_MB*N_MB; i++)
    {
        
        for (int edge = vertical; edge <= horizontal; edge++) // edge = 0 is vertical, edge = 1 is horizontal
        {
            for (int j = 0; j < N_edges; j++)
            {
                Bs = -1;
                if (((i+1) % N_MB) == 0 && j == 0 && edge == vertical) // vertical edge that is an end edge of a picture
                {
                    Bs = 0;
                }

                if (i == 0 && j == 0  && edge == horizontal) // horizontal edge that is an end edge of a picture
                {
                    Bs = 0;
                }

                if (Bs == -1){
                    Bs = rand() % 4 + 1;
                }

                if (edge == horizontal)
                {
                    ystride_luma = 1;
                    xstride_luma = XSTRIDE_LUMA;
                    ystride_chroma = 1;
                    xstride_chroma = XSTRIDE_CHROMA;
                    Y_p = Y_p_h;
                    Cb_p = Cb_p_h;
                    Cr_p = Cr_p_h;
                }
                else // vertical edge
                {
                    xstride_luma = 1;
                    ystride_luma = XSTRIDE_LUMA;
                    xstride_chroma = 1;
                    ystride_chroma = XSTRIDE_CHROMA;
                    Y_p = Y_p_v;
                    Cb_p = Cb_p_v;
                    Cr_p = Cr_p_v;
                }
#ifndef MAIN_MACRO 
                if (msa)
                {
                    deblock_luma_msa(Y_p, xstride_luma, ystride_luma, 0, 2, c1, Bs);
                    deblock_chroma_msa(Cb_p, Cr_p,  xstride_chroma, ystride_chroma, 0, 2, c1, Bs);
                }
                else
                {
                    deblock_luma(Y_p, xstride_luma, ystride_luma, 0, 2, c1, Bs);
                    deblock_chroma(Cb_p, Cr_p,  xstride_chroma, ystride_chroma, 0, 2, c1, Bs);
                }
#endif
                if (edge == vertical)
                {
                    Y_p_v += 4;
                    if (j == 0 || j == 2)
                    {
                        Cb_p_v += 4;
                        Cr_p_v += 4;
                    }
                }
                else
                {
                    Y_p_h += 4*XSTRIDE_LUMA;
                    if (j == 0 || j == 2)
                    {
                        Cb_p_h += 4*XSTRIDE_CHROMA;
                        Cr_p_h += 4*XSTRIDE_CHROMA;
                    }
                }
            }
        }
        Y_p_h = Y_p_v;
        Cb_p_h = Cb_p_v;
        Cr_p_h = Cr_p_v;
    }
}

int main()
{

    for(int i = 0; i < N_iter; i++)
    {
        generate_random_data();
#ifdef MSA
        deblock_area(1);
#else
        deblock_area(0);
#endif
    }
    return 0;
}
