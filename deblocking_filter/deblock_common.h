#ifndef DEBLOCK_COMMON_H
#define DEBLOCK_COMMON_H
#include <inttypes.h>
#define _ABS(a) ((((a) >> 31) + (a)) ^ ((a) >> 31))
#define _MAX(x,y) ((x) - (((x) - (y)) & (((x) - (y)) >> 31)))
#define _MIN(x,y) ((y) + (((x) - (y)) & (((x) - (y)) >> 31)))
#define _CLIP(d,c) _MIN(_MAX(-1*(c),(d)),(c))

// 0 -> 255 transition when there's arithmetic overflow
#define _BYTE_OVERFLOW(a) ((uint8_t) (((a) & 0xffffff00) ? (- (a) >> 31) : (a)))

// print any addressable element
#define print_vector(v,name,size,f)             \
    {                                           \
        uint8_t* p = (uint8_t*) &(v);           \
        printf("%s = [\n\t", (name));           \
        for(int i = 0; i < (size); i++)         \
        {                                       \
            if(f)                               \
            {                                   \
                printf("%.2x ", p[i]);          \
            }                                   \
            else                                \
            {                                   \
                printf("%d ", p[i]);            \
            }                                   \
            if((i+1)%8 == 0)                    \
                printf("\n\t");                 \
        }                                       \
        printf("]\n");                          \
    }                                           \

// print vector with int16_t elements
#define print_vector_i16(v,name,size,f)         \
    {                                           \
        int16_t* p = (int16_t*) &(v);           \
        printf("%s = [\n\t", (name));           \
        for(int i = 0; i < (size); i++)         \
        {                                       \
            if(f)                               \
            {                                   \
                printf("%.2x ", p[i]);          \
            }                                   \
            else                                \
            {                                   \
                printf("%d ", p[i]);            \
            }                                   \
            if((i+1)%8 == 0)                    \
                printf("\n\t");                 \
        }                                       \
        printf("]\n");                          \
    }                                           \

// print vector with int8_t elements
#define print_vector_i8(v,name,size,f)          \
    {                                           \
        int8_t* p = (int8_t*) &(v);             \
        printf("%s = [\n\t", (name));           \
        for(int i = 0; i < (size); i++)         \
        {                                       \
            if(f)                               \
            {                                   \
                printf("%.2x ", p[i]);          \
            }                                   \
            else                                \
            {                                   \
                printf("%d ", p[i]);            \
            }                                   \
            if((i+1)%8 == 0)                    \
                printf("\n\t");                 \
        }                                       \
        printf("]\n");                          \
    }                                           \

// compare two buffers
static inline int compare_two_buffers(uint8_t* buf1, uint8_t* buf2, int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        if ((buf1[i] ^ buf2[i]))
        {
            return 0;
        }
    }
    return 1;
}

#endif // DEBLOCK_COMMON_H
