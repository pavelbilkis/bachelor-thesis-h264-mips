#ifndef DEBLOCK_C_H
#define DEBLOCK_C_H
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

    /*
      Horizontal edge:
      p1 p1 p1
      p0 p0 p0
      --------
      q0 q0 q0
      p1 p1 p1

      Vertical edge:
      p1 p0 | q0 q1
      p1 p0 | q0 q1
      p1 p0 | q0 q1
    */
    
// public interfaces

    // deblock luma edge
    void deblock_luma (uint8_t* Y, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
    // deblock chroma edge 
    void deblock_chroma (uint8_t* Cb, uint8_t* Cr, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs);

// private

    // deblock luma horizontal edge
    void deblock_luma_h (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
    // deblock luma vertical edge
    void deblock_luma_v (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
    // deblock chroma vertical edge
    void deblock_chroma_v (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
    // deblock chroma horizontal edge
    void deblock_chroma_h (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
                    
// bs = 1,2,3
    void deblock_luma_edge_standart(uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t c1);
    void deblock_chroma_edge_standart(uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t c1);

// bs = 4
    void deblock_luma_edge_special(uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta);
    void deblock_chroma_edge_special(uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta);

#ifdef __cplusplus
}
#endif

#endif // DEBLOCK_C_H
