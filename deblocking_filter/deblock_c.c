#include <stdbool.h>
#include "deblock_c.h"
#include "deblock_common.h"
#include "stdio.h"
// deblock vertical luma edge
void deblock_luma_v (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    deblock_luma(Y, stride, 1, alpha, beta, c1, bs);
}
// deblock horizontal luma edge
void deblock_luma_h (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    deblock_luma(Y, 1, stride, alpha, beta, c1, bs);
}
// deblock vertical chroma edge
void deblock_chroma_v (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    deblock_chroma(Cb, Cr, stride, 1, alpha, beta, c1, bs);
}
// deblock horizontal chroma edge
void deblock_chroma_h (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    deblock_chroma(Cb, Cr, 1, stride, alpha, beta, c1, bs);
}
// deblock luma edge
void deblock_luma (uint8_t* Y, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    if(bs == 0) // no need for deblocking
        return;

    if(bs != 4)
    {
        if(c1 == NULL){
            printf("\nerror c1 is null\n");
            return;
        }
        int i;
        for (i = 0; i < 16; i++)
        {
            deblock_luma_edge_standart(Y, xstride, alpha, beta, c1[i >> 2]);
            Y += ystride;
        }

    }
    else
    {
        int i;
        for (i = 0; i < 16; i++)
        {
            deblock_luma_edge_special(Y, xstride, alpha, beta);
            Y += ystride;
        }
    }
}

void deblock_chroma (uint8_t* Cb, uint8_t* Cr, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    if(bs == 0)
        return;

    if(bs != 4)
    {
        if(c1 == NULL)
            return;
        int i;
        for (i = 0; i < 8; i++)
        {
            deblock_chroma_edge_standart(Cb, Cr, xstride, alpha, beta, c1[i >> 1]);
            Cb += ystride;
            Cr += ystride;
        }
    }
    else
    {
        int i;
        for (i = 0; i < 8; i++)
        {
            deblock_chroma_edge_special(Cb, Cr, xstride, alpha, beta);
            Cb += ystride;
            Cr += ystride;
        }
    }
}

// bs = 1,2,3

void deblock_luma_edge_standart(uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t c1)
{
    int32_t p0, q0, p1, q1;
    p0 = Y[-stride];
    p1 = Y[-2*stride];
    q0 = Y[0];
    q1 = Y[stride];
    bool p0q0, p1p0, q1q0;
    p0q0 = _ABS(p0 - q0) < alpha;
    p1p0 = _ABS(p1 - p0) < beta;
    q1q0 = _ABS(q1 - q0) < beta;

    if(p0q0 && p1p0 && q1q0)
    {
        int32_t p2, q2, c0;
        p2 = Y[-3*stride];
        q2 = Y[2*stride];
        bool p2p0, q2q0;
        p2p0 = _ABS(p2 - p0) < beta;
        q2q0 = _ABS(q2 - q0) < beta;
        c0 = c1 + p2p0 + q2q0;
        
        if(p2p0)
        {
            int32_t delta_p1_i, delta_p1;
            delta_p1_i = (p2 + ((p0 + q0 + 1) >> 1) - 2*p1) >> 1;
            delta_p1 = _CLIP(delta_p1_i, c1);
            Y[-2*stride] = p1 + delta_p1; // p1'
        }

        if(q2q0)
        {
            int32_t delta_q1_i, delta_q1;
            delta_q1_i = (q2 + ((p0 + q0 + 1) >> 1) - 2*q1) >> 1;
            delta_q1 = _CLIP(delta_q1_i, c1);
            Y[stride] = q1 + delta_q1; // q1'
        }

        int32_t delta_0_i, delta_0;
        delta_0_i = (4*(q0 - p0) + (p1 - q1) + 4) >> 3;
        delta_0 = _CLIP(delta_0_i, c0);
        Y[-stride] = _BYTE_OVERFLOW(p0 + delta_0); // p0'
        Y[0] = _BYTE_OVERFLOW(q0 - delta_0); // q0'
    }
}

static void deblock_chroma_edge_standart_single_part (uint8_t* C, int32_t stride, int32_t alpha, int32_t beta, int8_t c1)
{
    int32_t p0, q0, p1, q1;
    p0 = C[-stride];
    p1 = C[-2*stride];
    q0 = C[0];
    q1 = C[stride];
    bool p0q0, p1p0, q1q0;
    p0q0 = _ABS(p0 - q0) < alpha;
    p1p0 = _ABS(p1 - p0) < beta;
    q1q0 = _ABS(q1 - q0) < beta;

    if(p0q0 && p1p0 && q1q0)
    {
        int32_t delta_0_i, delta_0;
        delta_0_i = (4*(q0 - p0) + (p1 - q1) + 4) >> 3;
        delta_0 = _CLIP(delta_0_i, c1);
        C[-stride] = _BYTE_OVERFLOW(p0 + delta_0); // p0'
        C[0] = _BYTE_OVERFLOW(q0 - delta_0); // q0'
    }

}

void deblock_chroma_edge_standart(uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t c1)
{
    deblock_chroma_edge_standart_single_part(Cb, stride, alpha, beta, c1);
    deblock_chroma_edge_standart_single_part(Cr, stride, alpha, beta, c1);
}



// bs = 4

void deblock_luma_edge_special(uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta)
{
    int32_t p0, q0, p1, q1;
    p0 = Y[-stride];
    p1 = Y[-2*stride];
    q0 = Y[0];
    q1 = Y[stride];
    bool p0q0, p1p0, q1q0;
    p0q0 = _ABS(p0 - q0) < alpha;
    p1p0 = _ABS(p1 - p0) < beta;
    q1q0 = _ABS(q1 - q0) < beta;

    if(p0q0 && p1p0 && q1q0)
    {
        int32_t p3, q3, p2, q2;
        p2 = Y[-3*stride];
        q2 = Y[2*stride];
        p3 = Y[-4*stride];
        q3 = Y[3*stride];
        bool p2p0, q2q0;
        p2p0 = _ABS(p2 - p0) < beta;
        q2q0 = _ABS(q2 - q0) < beta;
        bool strong_p0q0 = _ABS(p0 - q0) < ((alpha >> 2) + 2);
        if(strong_p0q0)
        {
            if(p2p0)
            {
                Y[-stride] = (p2 + 2 * (p1 + p0 + q0) + q1 + 4) >> 3; // p0'
                Y[-2*stride] = (p2 + p1 + p0 + q0 + 2) >> 2; // p1'
                Y[-3*stride] = (2*p3 + 3*p2 + p1 + p0 + q0 + 4) >> 3; // p2'
            }
            else
            {
                
                Y[-stride] = (2*p1 + p0 + q1 + 2) >> 2; // p0'
            }

            if(q2q0)
            {
                Y[0] = (q2 + 2 * (q1 + p0 + q0) + p1 + 4) >> 3; // q0'
                Y[stride] = (q2 + q1 + q0 + p0 + 2) >> 2; // q1'
                Y[2*stride] = (2*q3 + 3*q2 + q1 + q0 + p0 + 4) >> 3; //q2'
            }
            else
            {
                Y[0] = (2*q1 + q0 + p1 + 2) >> 2; // q0'
            }
        }
        else
        {
            Y[-stride] = (2*p1 + p0 + q1 + 2) >> 2; // p0'
            Y[0] = (2*q1 + q0 + p1 + 2) >> 2; // q0'
        }
    }
}

static void deblock_chroma_edge_special_single_part(uint8_t* C, int32_t stride, int32_t alpha, int32_t beta)
{
    int32_t p0, q0, p1, q1;
    p0 = C[-stride];
    p1 = C[-2*stride];
    q0 = C[0];
    q1 = C[stride];
    bool p0q0, p1p0, q1q0;
    p0q0 = _ABS(p0 - q0) < alpha;
    p1p0 = _ABS(p1 - p0) < beta;
    q1q0 = _ABS(q1 - q0) < beta;

    if(p0q0 && p1p0 && q1q0)
    {
        C[-stride] = (2*p1 + p0 + q1 + 2) >> 2; // p0'
        C[0] = (2*q1 + q0 + p1 + 2) >> 2; // q0'
    }

}

void deblock_chroma_edge_special(uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta)
{

    deblock_chroma_edge_special_single_part(Cb, stride, alpha, beta);

    deblock_chroma_edge_special_single_part(Cr, stride, alpha, beta);
}
