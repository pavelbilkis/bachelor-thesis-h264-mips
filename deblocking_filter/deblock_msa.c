#include <msa.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "deblock_common.h"
#include "deblock_msa.h"
#include "deblock_c.h"
#include "deblock_msa_macros.h"

void deblock_luma_transpose_v_h (uint8_t* Y_v, uint8_t* Y_h, int32_t stride)
{
    deblock_luma_transpose_v_h_ref(Y_v, Y_h, stride);
}

void deblock_luma_transpose_h_v (uint8_t* Y_h, uint8_t* Y_v, int32_t stride)
{
    deblock_luma_transpose_h_v_ref(Y_h, Y_v, stride);
}

void deblock_luma_transpose_v_h_ref (uint8_t* Y_v, uint8_t* Y_h, int32_t stride)
{
    int i;
    for (i = 0; i < 16; i++)
    {
        Y_h[-16] = Y_v[-1]; // p0
        Y_h[-2*16] = Y_v[-2]; // p1
        Y_h[-3*16] = Y_v[-3]; // p2
        Y_h[-4*16] = Y_v[-4]; // p3
        Y_h[0] = Y_v[0]; // q0
        Y_h[16] = Y_v[1]; // q1
        Y_h[2*16] = Y_v[2]; // q2
        Y_h[3*16] = Y_v[3]; // q3
        Y_h++;
        Y_v += stride;
    }
}

void deblock_luma_transpose_h_v_ref (uint8_t* Y_h, uint8_t* Y_v, int32_t stride)
{
    int i;
    for (i = 0; i < 16; i++)
    {
        Y_v[-1] = Y_h[-16]; // p0
        Y_v[-2] = Y_h[-2*16]; // p1
        Y_v[-3] = Y_h[-3*16]; // p2
        Y_v[-4] = Y_h[-4*16]; // p3
        Y_v[0] = Y_h[0]; // q0
        Y_v[1] = Y_h[16]; // q1
        Y_v[2] = Y_h[2*16]; // q2
        Y_v[3] = Y_h[3*16]; // q3
        Y_h++;
        Y_v += stride;
    }
}

void deblock_chroma_transpose_v_h (uint8_t* Cb_v, uint8_t* Cb_h, uint8_t* Cr_v, uint8_t* Cr_h, int32_t stride)
{
    deblock_chroma_transpose_v_h_ref(Cb_v, Cb_h, Cr_v, Cr_h, stride);
}

void deblock_chroma_transpose_h_v (uint8_t* Cb_h, uint8_t* Cb_v, uint8_t* Cr_h, uint8_t* Cr_v, int32_t stride)
{
    deblock_chroma_transpose_h_v_ref(Cb_h, Cb_v, Cr_h, Cr_v, stride); 
}

void deblock_chroma_transpose_v_h_ref (uint8_t* Cb_v, uint8_t* Cb_h, uint8_t* Cr_v, uint8_t* Cr_h, int32_t stride)
{
    int i;
    for (i = 0; i < 8; i++)
    {
        // p0
        Cb_h[-1*8] = Cb_v[-1];
        Cr_h[-1*8] = Cr_v[-1];
        // p1
        Cb_h[-2*8] = Cb_v[-2];
        Cr_h[-2*8] = Cr_v[-2];
        
        // q0
        Cb_h[0] = Cb_v[0];
        Cr_h[0] = Cr_v[0];
        // q1
        Cb_h[8] = Cb_v[1];
        Cr_h[8] = Cr_v[1];
        
        Cb_h++;
        Cb_v += stride;
        Cr_h++;
        Cr_v += stride;
    }
}

void deblock_chroma_transpose_h_v_ref (uint8_t* Cb_h, uint8_t* Cb_v, uint8_t* Cr_h, uint8_t* Cr_v, int32_t stride)
{
    int i;
    for (i = 0; i < 8; i++)
    {
        // p0
        Cb_v[-1] = Cb_h[-1*8];
        Cr_v[-1] = Cr_h[-1*8];
        // p1
        Cb_v[-2] = Cb_h[-2*8];
        Cr_v[-2] = Cr_h[-2*8];
        
        // q0
        Cb_v[0] = Cb_h[0];
        Cr_v[0] = Cr_h[0];
        // q1
        Cb_v[1] = Cb_h[8];
        Cr_v[1] = Cr_h[8];
        
        Cb_h++;
        Cb_v += stride;
        Cr_h++;
        Cr_v += stride;
    }
}

void deblock_luma_v_edge_standart_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1)
{
    uint8_t Y_h[16*8] = {0};
    uint8_t* Y_h_p = &Y_h[16*4];
    deblock_luma_transpose_v_h(Y, Y_h_p, stride);
    deblock_luma_h_edge_standart_msa(Y_h_p, 16, alpha, beta, c1);
    deblock_luma_transpose_h_v(Y_h_p, Y, stride);
}

void deblock_luma_v_edge_special_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta)
{
    uint8_t Y_h[16*8] = {0};
    uint8_t* Y_h_p = &Y_h[16*4];
    deblock_luma_transpose_v_h(Y, Y_h_p, stride);
    deblock_luma_h_edge_special_msa(Y_h_p, 16, alpha, beta);
    deblock_luma_transpose_h_v(Y_h_p, Y, stride);
}

void deblock_chroma_v_edge_standart_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1)
{
    uint8_t Cb_h[8*4] = {0};
    uint8_t Cr_h[8*4] = {0};
    uint8_t* Cb_h_p = &Cb_h[8*2];
    uint8_t* Cr_h_p = &Cr_h[8*2];
    deblock_chroma_transpose_v_h(Cb, Cb_h_p, Cr, Cr_h_p, stride);
    deblock_chroma_h_edge_standart_msa(Cb_h_p, Cr_h_p, 8, alpha, beta, c1);
    deblock_chroma_transpose_h_v(Cb_h_p, Cb, Cr_h_p, Cr, stride);
}

void deblock_chroma_v_edge_special_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta)
{
    uint8_t Cb_h[8*4] = {0};
    uint8_t Cr_h[8*4] = {0};
    uint8_t* Cb_h_p = &Cb_h[8*2];
    uint8_t* Cr_h_p = &Cr_h[8*2];
    deblock_chroma_transpose_v_h(Cb, Cb_h_p, Cr, Cr_h_p, stride);
    deblock_chroma_h_edge_special_msa(Cb_h_p, Cr_h_p, 8, alpha, beta);
    deblock_chroma_transpose_h_v(Cb_h_p, Cb, Cr_h_p, Cr, stride);
}

void deblock_luma_h_edge_standart_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1)
{
    v16i8 p0_v = {0}, p1_v = {0}, p2_v = {0}, q0_v = {0}, q1_v = {0}, q2_v = {0};
    p0_v = __msa_ld_b(&Y[-stride], 0);
    p1_v = __msa_ld_b(&Y[-2*stride], 0);
    p2_v = __msa_ld_b(&Y[-3*stride], 0);

    q0_v = __msa_ld_b(Y, 0);
    q1_v = __msa_ld_b(&Y[stride], 0);
    q2_v = __msa_ld_b(&Y[2*stride], 0);

    v16i8 p0_f_v = {0}, p1_f_v = {0}, q0_f_v = {0}, q1_f_v = {0};
    //v16i8 p0_m_v = {0}, q0_m_v = {0}, p1_m_v = {0}, q1_m_v = {0};

    v16u8 mask;
    // conditions for filtering p0 and q0
    CHECK_MAIN_CONDITION(v16u8, mask, p0_v, p1_v, q0_v, q1_v, alpha, beta);

    v8i16 p0_f_v16[2], q0_f_v16[2], p1_f_v16[2], q1_f_v16[2], p2_f_v16[2], q2_f_v16[2];

    SE_BYTES_TO_HW_U(v8i16, p0_v, p0_f_v16[0], p0_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, p1_v, p1_f_v16[0], p1_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q0_v, q0_f_v16[0], q0_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q1_v, q1_f_v16[0], q1_f_v16[1]);

    v8i16 delta_i[2];
    FILTER_H_CHROMA_LUMA_v16(v8i16, delta_i[0], p0_f_v16[0], p1_f_v16[0], q0_f_v16[0], q1_f_v16[0]);
    FILTER_H_CHROMA_LUMA_v16(v8i16, delta_i[1], p0_f_v16[1], p1_f_v16[1], q0_f_v16[1], q1_f_v16[1]);

    v8i16 delta_0[2];
    v16i8 c0_v[2] = {{0},{0}}, c0_v_tmp = {0};
    uint32_t* c1_p = (uint32_t*) c1;

    c0_v_tmp = __msa_insert_w(c0_v_tmp, 0, c1_p[0]);
    c0_v_tmp = __msa_ilvr_b(c0_v_tmp, c0_v_tmp); // 0[a a b b c c d d | 0 0 0 0 0 0 0 0]15
    c0_v_tmp = __msa_ilvr_b(c0_v_tmp, c0_v_tmp); // 0[a a a a b b b b | c c c c d d d d]15
    SE_BYTES_TO_HW_S(v8i16, c0_v_tmp, c0_v[0], c0_v[1]);

    SE_BYTES_TO_HW_U(v8i16, p2_v, p2_f_v16[0], p2_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q2_v, q2_f_v16[0], q2_f_v16[1]);

    // conditions for filtering p1 and q1
    v16u8 p2p0_is_less_than_beta, q2q0_is_less_than_beta;
    v16u8 beta_v = (v16u8) __msa_fill_b(beta);
    v16u8 p2p0_v = (v16u8) __msa_asub_u_b((v16u8)p2_v, (v16u8)p0_v); /* |p2 - p0| */
    v16u8 q2q0_v = (v16u8) __msa_asub_u_b((v16u8)q2_v, (v16u8)q0_v); /* |q2 - q0| */
    p2p0_is_less_than_beta = (v16u8) __msa_clt_u_b(p2p0_v, beta_v);  /* |p2 - p0| < beta */
    q2q0_is_less_than_beta = (v16u8) __msa_clt_u_b(q2q0_v, beta_v);  /* |q2 - q0| < beta */
    p2p0_is_less_than_beta = __msa_and_v(p2p0_is_less_than_beta, mask);
    q2q0_is_less_than_beta = __msa_and_v(q2q0_is_less_than_beta, mask);

    // adding condition results to c0
    v16u8 c0_add = __msa_addv_b(__msa_srli_b(p2p0_is_less_than_beta, 7),
                                __msa_srli_b(q2q0_is_less_than_beta, 7));
    v8i16 _c0_v[2];
    SE_BYTES_TO_HW_U(v8i16, c0_add, _c0_v[0], _c0_v[1]);
    _c0_v[0] = __msa_adds_s_h(c0_v[0],
                              _c0_v[0]);
    _c0_v[1] = __msa_adds_s_h(c0_v[1],
                              _c0_v[1]);

    CLIPv16_MSA_LUMA(v8i16, delta_0, delta_i, _c0_v);

    v8i16 _p0_f_v16[2], _p1_f_v16[2], _q0_f_v16[2], _q1_f_v16[2];

    _p0_f_v16[0] = __msa_adds_s_h(p0_f_v16[0], delta_0[0]);
    _q0_f_v16[0] = __msa_subs_s_h(q0_f_v16[0], delta_0[0]);
    _p0_f_v16[1] = __msa_adds_s_h(p0_f_v16[1], delta_0[1]);
    _q0_f_v16[1] = __msa_subs_s_h(q0_f_v16[1], delta_0[1]);

    PACK_HW_TO_BYTES(v16i8, _p0_f_v16[0], _p0_f_v16[1], p0_f_v);
    PACK_HW_TO_BYTES(v16i8, _q0_f_v16[0], _q0_f_v16[1], q0_f_v);

    // fit in unfiltered values
    p0_f_v = (v16i8) __msa_bmz_v((v16u8)p0_f_v, (v16u8)p0_v, mask);
    q0_f_v = (v16i8) __msa_bmz_v((v16u8)q0_f_v, (v16u8)q0_v, mask);

    // filtering p1
    FILTER_H_LUMA_v16(v8i16, delta_i[0], p2_f_v16[0], p1_f_v16[0], p0_f_v16[0], q0_f_v16[0]);
    FILTER_H_LUMA_v16(v8i16, delta_i[1], p2_f_v16[1], p1_f_v16[1], p0_f_v16[1], q0_f_v16[1]);
    CLIPv16_MSA_LUMA(v8i16, delta_0, delta_i, c0_v);
    _p1_f_v16[0] = __msa_adds_s_h(p1_f_v16[0], delta_0[0]);
    _p1_f_v16[1] = __msa_adds_s_h(p1_f_v16[1], delta_0[1]);
    PACK_HW_TO_BYTES(v16i8, _p1_f_v16[0], _p1_f_v16[1], p1_f_v);

    // fit in unfiltered values
    p1_f_v = (v16i8) __msa_bmz_v((v16u8)p1_f_v, (v16u8)p1_v, p2p0_is_less_than_beta);


    // filtering q1
    FILTER_H_LUMA_v16(v8i16, delta_i[0], q2_f_v16[0], q1_f_v16[0], q0_f_v16[0], p0_f_v16[0]);
    FILTER_H_LUMA_v16(v8i16, delta_i[1], q2_f_v16[1], q1_f_v16[1], q0_f_v16[1], p0_f_v16[1]);
    CLIPv16_MSA_LUMA(v8i16, delta_0, delta_i, c0_v);
    _q1_f_v16[0] = __msa_adds_s_h(q1_f_v16[0], delta_0[0]);
    _q1_f_v16[1] = __msa_adds_s_h(q1_f_v16[1], delta_0[1]);
    PACK_HW_TO_BYTES(v16i8, _q1_f_v16[0], _q1_f_v16[1], q1_f_v);

    // fit in unfiltered values
    q1_f_v = (v16i8) __msa_bmz_v((v16u8)q1_f_v, (v16u8)q1_v, q2q0_is_less_than_beta);

    __msa_st_b(p0_f_v, &Y[-stride], 0); // store p0'
    __msa_st_b(p1_f_v, &Y[-2*stride], 0); // store p1'
    __msa_st_b(q0_f_v, Y, 0); // store q0'
    __msa_st_b(q1_f_v, &Y[stride], 0); // store q1'

    /* This was used for testing, may be useful later. */
    /* __msa_st_b(p0_f_v, &Yp[0], 0); // store p0' */
    /* __msa_st_b(p1_f_v, &Yp[16], 0); // store p1' */
    /* __msa_st_b(q0_f_v, &Yp[32], 0); // store q0' */
    /* __msa_st_b(q1_f_v, &Yp[48], 0); // store q1' */
}

void deblock_luma_h_edge_special_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta)
{
    v16i8 p0_v = {0}, p1_v = {0}, p2_v = {0}, p3_v = {0}, q0_v = {0}, q1_v = {0}, q2_v = {0}, q3_v = {3};
    p0_v = __msa_ld_b(&Y[-stride], 0);
    p1_v = __msa_ld_b(&Y[-2*stride], 0);
    p2_v = __msa_ld_b(&Y[-3*stride], 0);
    p3_v = __msa_ld_b(&Y[-4*stride], 0);

    q0_v = __msa_ld_b(Y, 0);
    q1_v = __msa_ld_b(&Y[stride], 0);
    q2_v = __msa_ld_b(&Y[2*stride], 0);
    q3_v = __msa_ld_b(&Y[3*stride], 0);

    v16i8 p0_f_v = {0}, p1_f_v = {0}, p2_f_v = {0}, q0_f_v = {0}, q1_f_v = {0}, q2_f_v = {0};
    v16i8 p0_m_v = {0}, q0_m_v = {0}, p1_m_v = {0}, q1_m_v = {0};

    v8i16 p0_f_v16[2], q0_f_v16[2], p1_f_v16[2], q1_f_v16[2], p2_f_v16[2], q2_f_v16[2], p3_f_v16[2], q3_f_v16[2];

    // Sign-extend byte values to word values for calculation purposes
    SE_BYTES_TO_HW_U(v8i16, p0_v, p0_f_v16[0], p0_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, p1_v, p1_f_v16[0], p1_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, p2_v, p2_f_v16[0], p2_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, p3_v, p3_f_v16[0], p3_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q0_v, q0_f_v16[0], q0_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q1_v, q1_f_v16[0], q1_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q2_v, q2_f_v16[0], q2_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q3_v, q3_f_v16[0], q3_f_v16[1]);

    v16u8 mask;
    CHECK_MAIN_CONDITION(v16u8, mask, p0_v, p1_v, q0_v, q1_v, alpha, beta);

    // masked
    p0_m_v = (v16i8) __msa_bmnz_v((v16u8)p0_m_v, (v16u8)p0_v, mask);
    p1_m_v = (v16i8) __msa_bmnz_v((v16u8)p1_m_v, (v16u8)p1_v, mask);
    q0_m_v = (v16i8) __msa_bmnz_v((v16u8)q0_m_v, (v16u8)q0_v, mask);
    q1_m_v = (v16i8) __msa_bmnz_v((v16u8)q1_m_v, (v16u8)q1_v, mask);

    // filter and compensate lost bits (due to right shift)
    // old code copied from deblock_chroma_h_edge_special_msa, so it doesn't use 16-bit values
    v16i8 two_v = __msa_fill_b(2);

    // p0'
    p0_f_v =(v16i8)  __msa_adds_u_b(__msa_adds_u_b((v16u8)__msa_srli_b(p0_m_v,2),
                                                   (v16u8)__msa_srli_b(q1_m_v,2)),
                                    __msa_adds_u_b((v16u8)__msa_srli_b(p1_m_v,2),
                                                   (v16u8)__msa_srli_b(p1_m_v,2))

        );

    v16u8 r_p0 = __msa_andi_b((v16u8)p0_m_v, 0x03);
    v16u8 r_q1 = __msa_andi_b((v16u8)q1_m_v, 0x03);
    v16u8 r_p1 = __msa_andi_b((v16u8)p1_m_v, 0x03);
    r_p1 =  __msa_adds_u_b(r_p1, r_p1); // 2*p1
    r_p1 =  __msa_adds_u_b(r_p1, (v16u8)two_v); // 2*p1 + 2
    v16u8 lost_bits =  (v16u8)__msa_srli_b((v16i8) __msa_adds_u_b(__msa_adds_u_b(r_p0, r_q1), r_p1),
                                           2);
    p0_f_v = (v16i8)  __msa_adds_u_b((v16u8) p0_f_v, lost_bits);

    // q0'
    q0_f_v =(v16i8)  __msa_adds_u_b(__msa_adds_u_b((v16u8)__msa_srli_b(q0_m_v,2),
                                                   (v16u8)__msa_srli_b(p1_m_v,2)),
                                    __msa_adds_u_b((v16u8)__msa_srli_b(q1_m_v,2),
                                                   (v16u8)__msa_srli_b(q1_m_v,2))
        );

    v16u8 r_q0 = __msa_andi_b((v16u8)q0_m_v, 0x03);
    r_q1 = __msa_andi_b((v16u8)q1_m_v, 0x03);
    r_p1 = __msa_andi_b((v16u8)p1_m_v, 0x03);
    r_q1 = __msa_adds_u_b(r_q1, r_q1); // 2*p1
    r_q1 = __msa_adds_u_b(r_q1, (v16u8)two_v); // 2*p1 + 2
    lost_bits =  (v16u8)__msa_srli_b((v16i8) __msa_adds_u_b(__msa_adds_u_b(r_q0, r_p1), r_q1),
                                     2);
    q0_f_v = (v16i8)  __msa_adds_u_b((v16u8) q0_f_v, lost_bits);

    // conditions (8) and (9)
    v16u8 p2p0_is_less_than_beta, q2q0_is_less_than_beta;
    v16u8 beta_v = (v16u8) __msa_fill_b(beta);
    v16u8 p2p0_v = (v16u8) __msa_asub_u_b((v16u8)p2_v, (v16u8)p0_v); /* |p2 - p0| */
    v16u8 q2q0_v = (v16u8) __msa_asub_u_b((v16u8)q2_v, (v16u8)q0_v); /* |q2 - q0| */
    p2p0_is_less_than_beta = (v16u8) __msa_clt_u_b(p2p0_v, beta_v);  /* |p2 - p0| < beta */
    q2q0_is_less_than_beta = (v16u8) __msa_clt_u_b(q2q0_v, beta_v);  /* |q2 - q0| < beta */
    p2p0_is_less_than_beta = __msa_and_v(p2p0_is_less_than_beta, mask);
    q2q0_is_less_than_beta = __msa_and_v(q2q0_is_less_than_beta, mask);
    // condition (19)
    // |p0 - q0| < (alpha >> 2) + 2
    v16u8 p0q0_strong;
    v16u8 alpha_shifted_v = (v16u8) __msa_srli_b(__msa_fill_b(alpha), 2); // (alpha >> 2)
    alpha_shifted_v = __msa_adds_u_b(alpha_shifted_v, two_v); // (alpha >> 2) + 2
    v16u8 p0q0_v = (v16u8) __msa_asub_u_b((v16u8)p0_v, (v16u8)q0_v); /* |p0 - q0| */
    p0q0_strong = (v16u8) __msa_clt_u_b(p0q0_v, alpha_shifted_v);  /* |p0 - q0| < (alpha >> 2) + 2 */

    // if 0x00 - corresponding value is to be filtered chroma way.
    // if not - special way, together with p1 and p2
    v16u8 p0_cond = __msa_and_v(p2p0_is_less_than_beta, p0q0_strong);
    v16u8 q0_cond = __msa_and_v(q2q0_is_less_than_beta, p0q0_strong);

    v16i8 p0_f_v_special = {0}, q0_f_v_special = {0};
    v8i16 _p0_f_v16[2], _p1_f_v16[2], _p2_f_v16[2],  _q0_f_v16[2], _q1_f_v16[2], _q2_f_v16[2];

    // special way of filtering p0 ((8) & (19) == T)
    // (p2 + 2 * (p1 + p0 + q0) + q1 + 4) >> 3
    FILTER_H_LUMA_SPECIAL_P0Q0_v16(v8i16, _p0_f_v16[0], p0_f_v16[0], p1_f_v16[0], p2_f_v16[0], q0_f_v16[0], q1_f_v16[0]);
    FILTER_H_LUMA_SPECIAL_P0Q0_v16(v8i16, _p0_f_v16[1], p0_f_v16[1], p1_f_v16[1], p2_f_v16[1], q0_f_v16[1], q1_f_v16[1]);
    PACK_HW_TO_BYTES(v16i8, _p0_f_v16[0], _p0_f_v16[1], p0_f_v_special);

    // special way of filtering q0 ((9) & (19) == T)
    FILTER_H_LUMA_SPECIAL_P0Q0_v16(v8i16, _q0_f_v16[0], q0_f_v16[0], q1_f_v16[0], q2_f_v16[0], p0_f_v16[0], p1_f_v16[0]);
    FILTER_H_LUMA_SPECIAL_P0Q0_v16(v8i16, _q0_f_v16[1], q0_f_v16[1], q1_f_v16[1], q2_f_v16[1], p0_f_v16[1], p1_f_v16[1]);
    PACK_HW_TO_BYTES(v16i8, _q0_f_v16[0], _q0_f_v16[1], q0_f_v_special);

    // special way of wiltering p1, p2 ((8) & (19) == T)
    FILTER_H_LUMA_SPECIAL_P1P2_v16(v8i16, _p1_f_v16[0], _p2_f_v16[0], p0_f_v16[0], p1_f_v16[0], p2_f_v16[0], p3_f_v16[0], q0_f_v16[0], q1_f_v16[0]);
    FILTER_H_LUMA_SPECIAL_P1P2_v16(v8i16, _p1_f_v16[1], _p2_f_v16[1], p0_f_v16[1], p1_f_v16[1], p2_f_v16[1], p3_f_v16[1], q0_f_v16[1], q1_f_v16[1]);
    PACK_HW_TO_BYTES(v16i8, _p1_f_v16[0], _p1_f_v16[1], p1_f_v);
    PACK_HW_TO_BYTES(v16i8, _p2_f_v16[0], _p2_f_v16[1], p2_f_v);

    // special way of filtering q1, q2 ((9) & (19) == T)
    FILTER_H_LUMA_SPECIAL_P1P2_v16(v8i16, _q1_f_v16[0], _q2_f_v16[0], q0_f_v16[0], q1_f_v16[0], q2_f_v16[0], q3_f_v16[0], p0_f_v16[0], p1_f_v16[0]);
    FILTER_H_LUMA_SPECIAL_P1P2_v16(v8i16, _q1_f_v16[1], _q2_f_v16[1], q0_f_v16[1], q1_f_v16[1], q2_f_v16[1], q3_f_v16[1], p0_f_v16[1], p1_f_v16[1]);
    PACK_HW_TO_BYTES(v16i8, _q1_f_v16[0], _q1_f_v16[1], q1_f_v);
    PACK_HW_TO_BYTES(v16i8, _q2_f_v16[0], _q2_f_v16[1], q2_f_v);

    // move according to the conditioned filtering
    p0_f_v = (v16i8) __msa_bmnz_v((v16u8)p0_f_v, (v16u8)p0_f_v_special, p0_cond);
    q0_f_v = (v16i8) __msa_bmnz_v((v16u8)q0_f_v, (v16u8)q0_f_v_special, q0_cond);

    // fit in unfiltered values
    p0_f_v = (v16i8) __msa_bmz_v((v16u8)p0_f_v, (v16u8)p0_v, mask);
    p1_f_v = (v16i8) __msa_bmz_v((v16u8)p1_f_v, (v16u8)p1_v, p0_cond);
    p2_f_v = (v16i8) __msa_bmz_v((v16u8)p2_f_v, (v16u8)p2_v, p0_cond);
    q0_f_v = (v16i8) __msa_bmz_v((v16u8)q0_f_v, (v16u8)q0_v, mask);
    q1_f_v = (v16i8) __msa_bmz_v((v16u8)q1_f_v, (v16u8)q1_v, q0_cond);
    q2_f_v = (v16i8) __msa_bmz_v((v16u8)q2_f_v, (v16u8)q2_v, q0_cond);

    __msa_st_b(p0_f_v, &Y[-stride], 0); // store p0'
    __msa_st_b(p1_f_v, &Y[-2*stride], 0); // store p1'
    __msa_st_b(p2_f_v, &Y[-3*stride], 0); // store p2'
    __msa_st_b(q0_f_v, Y, 0); // store q0'
    __msa_st_b(q1_f_v, &Y[stride], 0); // store q1'
    __msa_st_b(q2_f_v, &Y[2*stride], 0); // store q2'

}

void deblock_chroma_h_edge_standart_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1)
{
    // [Cb Cr]
    v16i8 p0_v = {0}, p1_v = {0}, q0_v = {0}, q1_v = {0};
    LD_16B_TWO_SOURCES(v16i8, p0_v, &Cb[-stride], &Cr[-stride]);
    LD_16B_TWO_SOURCES(v16i8, p1_v, &Cb[-2*stride], &Cr[-2*stride]);
    LD_16B_TWO_SOURCES(v16i8, q0_v, Cb, Cr);
    LD_16B_TWO_SOURCES(v16i8, q1_v, &Cb[stride], &Cr[stride]);

    v16i8 p0_f_v = {0}, q0_f_v = {0};
    v16i8 p0_m_v = {0}, q0_m_v = {0}, p1_m_v = {0}, q1_m_v = {0};

    v16u8 mask;
    CHECK_MAIN_CONDITION(v16u8, mask, p0_v, p1_v, q0_v, q1_v, alpha, beta);

    // masked
    p0_m_v = (v16i8) __msa_bmnz_v((v16u8)p0_m_v, (v16u8)p0_v, mask);
    p1_m_v = (v16i8) __msa_bmnz_v((v16u8)p1_m_v, (v16u8)p1_v, mask);
    q0_m_v = (v16i8) __msa_bmnz_v((v16u8)q0_m_v, (v16u8)q0_v, mask);
    q1_m_v = (v16i8) __msa_bmnz_v((v16u8)q1_m_v, (v16u8)q1_v, mask);

    v8i16 p0_f_v16[2], q0_f_v16[2], p1_f_v16[2], q1_f_v16[2];
    v16i8 c0_v = {0} ;

    SE_BYTES_TO_HW_U(v8i16, p0_m_v, p0_f_v16[0], p0_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, p1_m_v, p1_f_v16[0], p1_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q0_m_v, q0_f_v16[0], q0_f_v16[1]);
    SE_BYTES_TO_HW_U(v8i16, q1_m_v, q1_f_v16[0], q1_f_v16[1]);

    v8i16 delta_i[2];
    FILTER_H_CHROMA_LUMA_v16(v8i16, delta_i[0], p0_f_v16[0], p1_f_v16[0], q0_f_v16[0], q1_f_v16[0]);
    FILTER_H_CHROMA_LUMA_v16(v8i16, delta_i[1], p0_f_v16[1], p1_f_v16[1], q0_f_v16[1], q1_f_v16[1]);

    v8i16 delta_0[2];
    uint32_t* c1_p = (uint32_t*) c1;
    c0_v = __msa_insert_w(c0_v, 0, c1_p[0]);
    c0_v = __msa_ilvr_b(c0_v, c0_v);
    v16i8 sign;
    sign = __msa_clti_s_b((v16i8)c0_v, 0);
    c0_v =  __msa_ilvr_b(sign, (v16i8)c0_v);
    CLIPv16_MSA(v8i16, delta_0, delta_i, c0_v);

    p0_f_v16[0] = __msa_adds_s_h(p0_f_v16[0], delta_0[0]);
    q0_f_v16[0] = __msa_subs_s_h(q0_f_v16[0], delta_0[0]);
    p0_f_v16[1] = __msa_adds_s_h(p0_f_v16[1], delta_0[1]);
    q0_f_v16[1] = __msa_subs_s_h(q0_f_v16[1], delta_0[1]);

    /* p0_f_v16[0] = __msa_sat_u_h(p0_f_v16[0], 7); */
    /* q0_f_v16[0] = __msa_sat_u_h(q0_f_v16[0], 7); */
    /* p0_f_v16[1] = __msa_sat_u_h(p0_f_v16[1], 7); */
    /* q0_f_v16[1] = __msa_sat_u_h(q0_f_v16[1], 7); */
    

    PACK_HW_TO_BYTES(v16i8, p0_f_v16[0], p0_f_v16[1], p0_f_v);
    PACK_HW_TO_BYTES(v16i8, q0_f_v16[0], q0_f_v16[1], q0_f_v);

    // fit in unfiltered values
    p0_f_v = (v16i8) __msa_bmz_v((v16u8)p0_f_v, (v16u8)p0_v, mask);
    q0_f_v = (v16i8) __msa_bmz_v((v16u8)q0_f_v, (v16u8)q0_v, mask);

    ST_16B_TWO_LOCATIONS(p0_f_v, &Cb[-stride], &Cr[-stride]);
    ST_16B_TWO_LOCATIONS(q0_f_v, &Cb[0], &Cr[0]);
}

void deblock_chroma_h_edge_special_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta)
{
    // [Cb Cr]
    v16i8 p0_v = {0}, p1_v = {0}, q0_v = {0}, q1_v = {0};
    LD_16B_TWO_SOURCES(v16i8, p0_v, &Cb[-stride], &Cr[-stride]);
    LD_16B_TWO_SOURCES(v16i8, p1_v, &Cb[-2*stride], &Cr[-2*stride]);
    LD_16B_TWO_SOURCES(v16i8, q0_v, Cb, Cr);
    LD_16B_TWO_SOURCES(v16i8, q1_v, &Cb[stride], &Cr[stride]);

    v16i8 p0_f_v = {0}, q0_f_v = {0};
    v16i8 p0_m_v = {0}, q0_m_v = {0}, p1_m_v = {0}, q1_m_v = {0};

    v16u8 mask;
    CHECK_MAIN_CONDITION(v16u8, mask, p0_v, p1_v, q0_v, q1_v, alpha, beta);

    // masked
    p0_m_v = (v16i8) __msa_bmnz_v((v16u8)p0_m_v, (v16u8)p0_v, mask);
    p1_m_v = (v16i8) __msa_bmnz_v((v16u8)p1_m_v, (v16u8)p1_v, mask);
    q0_m_v = (v16i8) __msa_bmnz_v((v16u8)q0_m_v, (v16u8)q0_v, mask);
    q1_m_v = (v16i8) __msa_bmnz_v((v16u8)q1_m_v, (v16u8)q1_v, mask);

    // filter and compensate lost bits (due to right shift)
    v16i8 two_v = __msa_fill_b(2);

    // p0'
    p0_f_v =(v16i8)  __msa_adds_u_b(__msa_adds_u_b((v16u8)__msa_srli_b(p0_m_v,2),
                                                   (v16u8)__msa_srli_b(q1_m_v,2)),
                                    __msa_adds_u_b((v16u8)__msa_srli_b(p1_m_v,2),
                                                   (v16u8)__msa_srli_b(p1_m_v,2))

        );

    v16u8 r_p0 = __msa_andi_b((v16u8)p0_m_v, 0x03);
    v16u8 r_q1 = __msa_andi_b((v16u8)q1_m_v, 0x03);
    v16u8 r_p1 = __msa_andi_b((v16u8)p1_m_v, 0x03);
    r_p1 =  __msa_adds_u_b(r_p1, r_p1); // 2*p1
    r_p1 =  __msa_adds_u_b(r_p1, (v16u8)two_v); // 2*p1 + 2
    v16u8 lost_bits =  (v16u8)__msa_srli_b((v16i8) __msa_adds_u_b(__msa_adds_u_b(r_p0, r_q1), r_p1),
                                           2);
    p0_f_v = (v16i8)  __msa_adds_u_b((v16u8) p0_f_v, lost_bits);

    // q0'
    q0_f_v =(v16i8)  __msa_adds_u_b(__msa_adds_u_b((v16u8)__msa_srli_b(q0_m_v,2),
                                                   (v16u8)__msa_srli_b(p1_m_v,2)),
                                    __msa_adds_u_b((v16u8)__msa_srli_b(q1_m_v,2),
                                                   (v16u8)__msa_srli_b(q1_m_v,2))
        );

    v16u8 r_q0 = __msa_andi_b((v16u8)q0_m_v, 0x03);
    r_q1 = __msa_andi_b((v16u8)q1_m_v, 0x03);
    r_p1 = __msa_andi_b((v16u8)p1_m_v, 0x03);
    r_q1 =  __msa_adds_u_b(r_q1, r_q1); // 2*p1
    r_q1 =  __msa_adds_u_b(r_q1, (v16u8)two_v); // 2*p1 + 2
    lost_bits =  (v16u8)__msa_srli_b((v16i8) __msa_adds_u_b(__msa_adds_u_b(r_q0, r_p1), r_q1),
                                     2);
    q0_f_v = (v16i8)  __msa_adds_u_b((v16u8) q0_f_v, lost_bits);

    // fit in unfiltered values
    p0_f_v = (v16i8) __msa_bmz_v((v16u8)p0_f_v, (v16u8)p0_v, mask);
    q0_f_v = (v16i8) __msa_bmz_v((v16u8)q0_f_v, (v16u8)q0_v, mask);

    // store
    ST_16B_TWO_LOCATIONS(p0_f_v, &Cb[-stride], &Cr[-stride]);
    ST_16B_TWO_LOCATIONS(q0_f_v, &Cb[0], &Cr[0]);
}

void deblock_luma_msa (uint8_t* Y, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    if(bs == 0)
        return;
    
    if(bs != 4)
    {
        if(c1 == NULL)
            return;
        
        if(ystride == 1)
        {
            deblock_luma_h_edge_standart_msa(Y, xstride, alpha, beta, c1);
        }
        else
        {
            deblock_luma_v_edge_standart_msa(Y, ystride, alpha, beta, c1);
        }

    }
    else
    {
        if(ystride == 1)
        {
            deblock_luma_h_edge_special_msa(Y, xstride, alpha, beta);
        }
        else
        {
            deblock_luma_v_edge_special_msa(Y, ystride, alpha, beta);
        }
    }
}

void deblock_chroma_msa (uint8_t* Cb, uint8_t* Cr, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs)
{
    if(bs == 0)
        return;

    if(bs != 4)
    {
        if(c1 == NULL)
            return;

        if(ystride == 1)
        {
            deblock_chroma_h_edge_standart_msa(Cb, Cr, xstride, alpha, beta, c1);
        }
        else
        {
            deblock_chroma_v_edge_standart_msa(Cb, Cr, ystride, alpha, beta, c1);
        }
    }
    else
    {
        if(ystride == 1)
        {
            deblock_chroma_h_edge_special_msa(Cb, Cr, xstride, alpha, beta);
        }
        else
        {
            deblock_chroma_v_edge_special_msa(Cb, Cr, ystride, alpha, beta);
        }
    }
}
