#ifndef DEBLOCK_MSA_H
#define DEBLOCK_MSA_H
#include <inttypes.h>

/*
  Horizontal edge:
  p1 p1 p1
  p0 p0 p0
  --------
  q0 q0 q0
  p1 p1 p1

  Vertical edge:
  p1 p0 | q0 q1
  p1 p0 | q0 q1
  p1 p0 | q0 q1
*/

#ifdef __cplusplus
extern "C" {
#endif

    void deblock_luma_msa (uint8_t* Y, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
    void deblock_chroma_msa (uint8_t* Cb, uint8_t* Cr, int32_t xstride, int32_t ystride, int32_t alpha, int32_t beta, int8_t* c1, int bs);
    void deblock_luma_transpose_v_h (uint8_t* Y_v, uint8_t* Y_h, int32_t stride);
    void deblock_luma_transpose_h_v (uint8_t* Y_h, uint8_t* Y_v, int32_t stride);
    void deblock_luma_transpose_v_h_ref (uint8_t* Y_v, uint8_t* Y_h, int32_t stride);
    void deblock_luma_transpose_h_v_ref (uint8_t* Y_h, uint8_t* Y_v, int32_t stride);

    void deblock_chroma_transpose_h_v (uint8_t* Cb_h, uint8_t* Cb_v, uint8_t* Cr_h, uint8_t* Cr_v, int32_t stride);
    void deblock_chroma_transpose_v_h (uint8_t* Cb_v, uint8_t* Cb_h, uint8_t* Cr_v, uint8_t* Cr_h, int32_t stride);
    void deblock_chroma_transpose_h_v_ref (uint8_t* Cb_h, uint8_t* Cb_v, uint8_t* Cr_h, uint8_t* Cr_v, int32_t stride);
    void deblock_chroma_transpose_v_h_ref (uint8_t* Cb_v, uint8_t* Cb_h, uint8_t* Cr_v, uint8_t* Cr_h, int32_t stride);
    
// bs = 1,2,3

    void deblock_luma_h_edge_standart_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1);
    void deblock_luma_v_edge_standart_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1);
    //void deblock_luma_h_edge_standart_msa (uint8_t* Y,uint8_t* Yp, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1);
    void deblock_chroma_h_edge_standart_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1);
    void deblock_chroma_v_edge_standart_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta, int8_t* c1);

// bs = 4
    //void deblock_luma_h_edge_special_msa (uint8_t* Y, uint8_t* Yp, int32_t stride, int32_t alpha, int32_t beta);
    void deblock_luma_h_edge_special_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta);
    void deblock_luma_v_edge_special_msa (uint8_t* Y, int32_t stride, int32_t alpha, int32_t beta);
    void deblock_chroma_h_edge_special_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta);
    void deblock_chroma_v_edge_special_msa (uint8_t* Cb, uint8_t* Cr, int32_t stride, int32_t alpha, int32_t beta);

#ifdef __cplusplus
}
#endif

#endif // DEBLOCK_MSA_H
