#ifndef DEBLOCK_MSA_MACROS_H
#define DEBLOCK_MSA_MACROS_H

/*
  Load 4 words (16 bytes) from memory to vector with a stride
  Assuming:
  v4i32 out_v
  uint8_t* in
  int stride
*/
#define LD_4W_STRIDE(RTYPE, out_v, in, stride)                          \
    {                                                                   \
        out_v = (RTYPE)__msa_insert_w((v4i32)(out_v), 0, *((uint32_t*)(in))); \
        out_v = (RTYPE)__msa_insert_w((v4i32)(out_v), 1, *((uint32_t*)((in)+(stride)))); \
        out_v = (RTYPE)__msa_insert_w((v4i32)(out_v), 2, *((uint32_t*)((in)+2*(stride)))); \
        out_v = (RTYPE)__msa_insert_w((v4i32)(out_v), 3, *((uint32_t*)((in)+3*(stride)))); \
    }                                                                   \

/*
  Load low vector half (8 byte) from in1, and high half from in2.
*/
#define LD_16B_TWO_SOURCES(RTYPE, out_v, in1, in2)                      \
    {                                                                   \
        out_v = (RTYPE) __msa_insert_d((v2i64)(out_v), 0, *((uint64_t*)(in1))); \
        out_v = (RTYPE) __msa_insert_d((v2i64)(out_v), 1, *((uint64_t*)(in2))); \
    }                                                                   \

#define ST_16B_TWO_LOCATIONS(out_v, in1, in2)                           \
    {                                                                   \
        *((uint64_t*)(in1)) = __msa_copy_u_d((v2i64)(out_v), 0);        \
        *((uint64_t*)(in2)) = __msa_copy_u_d((v2i64)(out_v), 1);        \
    }                                                                   \

#define CHECK_MAIN_CONDITION(RTYPE, out_mask, p0_v, p1_v, q0_v, q1_v, alpha, beta) \
    {                                                                   \
        v16u8 alpha_v = (v16u8) __msa_fill_b(alpha);                    \
        v16u8 beta_v = (v16u8) __msa_fill_b(beta);                      \
        v16u8 is_less_than_alpha, is_less_than_beta;                    \
        v16u8 p0q0_v = (v16u8) __msa_asub_u_b((v16u8)p0_v, (v16u8)q0_v); /* |p0 - q0| */ \
        v16u8 p1p0_v = (v16u8) __msa_asub_u_b((v16u8)p1_v, (v16u8)p0_v); /* |p1 - p0| */ \
        v16u8 q1q0_v = (v16u8) __msa_asub_u_b((v16u8)q1_v, (v16u8)q0_v); /* |q1 - q0| */ \
        is_less_than_alpha = (v16u8) __msa_clt_u_b(p0q0_v, alpha_v); /* |p0 - q0| < alpha */ \
        is_less_than_beta = (v16u8) __msa_and_v((v16u8)__msa_clt_u_b(p1p0_v, beta_v), \
                                                (v16u8)__msa_clt_u_b(q1q0_v, beta_v)); /* (|p1 - p0| < beta) && (|q1 - q0| < beta) */ \
        out_mask = (RTYPE) __msa_and_v(is_less_than_alpha, is_less_than_beta); /* (|p0 - q0| < alpha) && (|p1 - p0| < beta) && (|q1 - q0| < beta) */ \
    }                                                                   \

/*
  Sign-extend bytes from input vector to halfwords in two output vectors
*/
#define SE_BYTES_TO_HW_S(RTYPE, in, out1, out2)         \
    {                                                   \
        v16i8 sign;                                     \
        sign = __msa_clti_s_b((v16i8)in, 0);            \
        out1 = (RTYPE) __msa_ilvr_b(sign, (v16i8)in);   \
        out2 = (RTYPE) __msa_ilvl_b(sign, (v16i8)in);   \
    }
\

#define SE_BYTES_TO_HW_U(RTYPE, in, out1, out2)         \
    {                                                   \
        v16i8 sign;                                     \
        sign = __msa_clti_u_b((v16i8)in, 0);            \
        out1 = (RTYPE) __msa_ilvr_b(sign, (v16i8)in);   \
        out2 = (RTYPE) __msa_ilvl_b(sign, (v16i8)in);   \
    }                                                   \

/*
  Pack halfwords from two input vectors to bytes in output vector.
*/

#define PACK_HW_TO_BYTES(RTYPE, in1, in2, out)                  \
    {                                                           \
        out = (RTYPE) __msa_pckev_b((v16i8)in2, (v16i8)in1);    \
    }                                                           \

/*
  Clip one lane (one vector) of delta_i and one lane of c0_v
  RTYPE d0;
  RTYPE di;
  RTYPE c0_v;
*/
#define CLIPv16_MSA_BASE(RTYPE, d0, di, c0_v)                           \
    {                                                                   \
        v16i8 minus_c0_v = __msa_mulv_h((v16i8)__msa_fill_h(-1), c0_v); \
        d0 = (RTYPE) __msa_min_s_h(__msa_max_s_h(di, minus_c0_v), c0_v); \
    }                                                                   \

/*
  Clip delta_i (di) values.
  Same as CLIPv16_MSA, but c0_v is different for di[0] and di[1], because in CLIPv16_MSA (chroma) right half is for Cb (8 bytes) value
  and left half is for Cr (8 bytes) values, so they have the same c0_v vector, but in the luma case,
  we're operating on 16 luma values.
  RTYPE d0[2];
  RTYPE di[2];
  RTYPE c0_v[2];
*/
#define CLIPv16_MSA_LUMA(RTYPE, d0, di, c0_v)           \
    {                                                   \
        CLIPv16_MSA_BASE(RTYPE, d0[0], di[0], c0_v[0]); \
        CLIPv16_MSA_BASE(RTYPE, d0[1], di[1], c0_v[1]); \
    }                                                   \

/*
  Clip delta_i (di) values.
  RTYPE d0[2];
  RTYPE di[2];
  RTYPE c0_v;
*/
#define CLIPv16_MSA(RTYPE, d0, di, c0_v)                                \
    {                                                                   \
        v16i8 minus_c0_v = __msa_mulv_h((v16i8)__msa_fill_h(-1), c0_v); \
        d0[0] = (RTYPE) __msa_min_s_h(__msa_max_s_h(di[0], minus_c0_v), c0_v); \
        d0[1] = (RTYPE) __msa_min_s_h(__msa_max_s_h(di[1], minus_c0_v), c0_v); \
    }                                                                   \


#define FILTER_H_CHROMA_LUMA_v16(RTYPE, di, p0, p1, q0, q1)             \
    {                                                                   \
        v8i16 four_v = __msa_fill_h(4);                                 \
        di = (RTYPE)__msa_maddv_h(__msa_subs_s_h(q0, p0),               \
                                  four_v,                               \
                                  __msa_subs_s_h(p1, q1)                \
            ); /* 4*(q0 - p0) + (p1 - q1) */                            \
        di = (RTYPE) __msa_adds_s_h(di, four_v); /* 4*(q0 - p0) + (p1 - q1) + 4 */ \
        di = (RTYPE) __msa_srai_h(di, 3); /* (4*(q0 - p0) + (p1 - q1)) >> 3 */ \
    }                                                                   \

// di = (x2 + ((x0 + y0 + 1) >> 1) - 2*x1  >> 1
#define FILTER_H_LUMA_v16(RTYPE, di, x2, x1, x0, y0)                    \
    {                                                                   \
        v8i16 one_v = __msa_fill_h(1);                                  \
        di = (RTYPE) __msa_adds_s_h(x2,                                 \
                                    __msa_srai_h(__msa_adds_s_h(        \
                                                     x0,                \
                                                     __msa_adds_s_h(y0, one_v) \
                                                     ),                 \
                                                 1)                     \
            ); /* x2 + ((x0 + y0 + 1) >> 1) */                          \
        di = (RTYPE) __msa_subs_s_h(di, x1); /* (x2 + ((x0 + y0 + 1) >> 1) - x1) */ \
        di = (RTYPE) __msa_subs_s_h(di, x1); /* (x2 + ((x0 + y0 + 1) >> 1) - 2*x1) */ \
        di = (RTYPE) __msa_srai_h(di, 1); /* (x2 + ((x0 + y0 + 1) >> 1) - 2*x1  >> 1 */ \
    }                                                                   \

#define FILTER_H_LUMA_SPECIAL_P0Q0_v16(RTYPE, out_v, p0, p1, p2, q0, q1) \
    {                                                                   \
        v8u16 two_v = __msa_fill_h(2);                                  \
        out_v = (RTYPE) __msa_maddv_h(two_v,                            \
                                      __msa_adds_s_h(p1,                \
                                                     __msa_adds_s_h(p0, q0)), \
                                      __msa_adds_s_h(p2,                \
                                                     __msa_addvi_h(q1, 4))); \
        out_v = (RTYPE) __msa_srai_h(out_v, 3);                         \
    }                                                                   \

// p1' = (p2 + p1 + p0 + q0 + 2) >> 2
// p2' = (2*p3 + 3*p2 + p1 + p0 + q0 + 4) >> 3
#define FILTER_H_LUMA_SPECIAL_P1P2_v16(RTYPE, p1_f, p2_f, p0, p1, p2, p3, q0, q1) \
    {                                                                   \
        v8u16 two_v = __msa_fill_h(2);                                  \
        v8u16 three_v = __msa_fill_h(3);                                \
        v8i16 p1p0q0 = __msa_adds_s_h(__msa_adds_s_h(p1, p0),           \
                                      q0);                              \
        p1_f = (RTYPE) __msa_adds_s_h(__msa_addvi_h(p1p0q0, 2), p2);    \
        p1_f = (RTYPE) __msa_srai_h(p1_f, 2);                           \
        p2_f = (RTYPE) __msa_maddv_h(p3,                                \
                                     two_v,                             \
                                     __msa_maddv_h(p2,                  \
                                                   three_v,             \
                                                   __msa_addvi_h(p1p0q0, 4))); \
        p2_f = (RTYPE) __msa_srai_h(p2_f, 3);                           \
    }                                                                   \

#endif // DEBLOCK_MSA_MACROS_H
